import * as React from 'react'
import {useState} from 'react'

const Context = React.createContext({});

export function CompaniesContextProvider({children}) {
	const [state, setState] = useState({
	    list: []
	});

	return (
		<Context.Provider value={{state, setState}}>
			{children}
		</Context.Provider>
	);
}

export default Context;