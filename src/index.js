import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.scss';
import {Toaster} from 'react-hot-toast'
import CommonProviders from "./Providers";
import {CompaniesList} from "./components";

import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
library.add(fab);
library.add(far);
library.add(fas);

ReactDOM.render(
    <CommonProviders>
        <Toaster />
        <CompaniesList />
    </CommonProviders>,
  document.getElementById('root')
);