import * as React from "react"
import toast from 'react-hot-toast'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function useToaster() {
    const iconStyles = {
            borderRadius: '50%',
            color: 'white',
            width: '20px',
            height: '20px',
            padding: '4px'
    }
    const iconInfo = <FontAwesomeIcon style={{
            background: "var(--blue)",
            ...iconStyles
        }} icon={['fas', 'info']} />
    const iconWarning = <FontAwesomeIcon style={{
            background: "var(--yellow)",
            ...iconStyles
        }} icon={['fas', 'exclamation-triangle']} />

    const showToaster = {
        default: (msg='', props={}) => toast(msg, props),
		info: (msg='', props={}) => toast(msg, {icon: iconInfo, ...props}),
		warning: (msg='', props={}) => toast(msg, {icon: iconWarning, ...props}),
		success: (msg='Ok', props={}) => toast.success(msg, props),
		error: (msg='Error', props={}) => toast.error(msg, props),
        promise: (myPromise, props={}, styles={}) => toast.promise(myPromise, {
            loading: 'Loading',
            success: 'Got the data',
            error: 'Error when fetching',
            ...props
        }, styles)
    }
    return {showToaster};
}