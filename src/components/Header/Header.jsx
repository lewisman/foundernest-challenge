import * as React from "react"
import {useCompanies} from "../../hooks";
import "./header.scss"

export default function Header() {
    const {list} = useCompanies()

    return <div className="HeaderComponent">

        <h2 className="title">Discover {list.length > 0 && `(${list.length})`}</h2>

        <div className="actions">
            <span>Your Network has <b>0</b> members!</span>
            <button>Invite my network</button>
        </div>
    </div>
}
