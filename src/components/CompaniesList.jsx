import * as React from "react"
import {useCompanies} from "../hooks";
import Header from "./Header/Header";
import CompanyCard from "./Companies/CompanyCard";
import CompanyFilter from "./Companies/CompanyFilter";
import {useEffect} from "react";

export default function CompaniesList() {
    const {list, updateList} = useCompanies()

    useEffect(() => {
        updateList()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return <>
        <Header />
        <CompanyFilter />
        <div className="CompaniesWraper">
            {list.map((company, idx) => <CompanyCard key={idx} companyInfo={company} />)}
        </div>

    </>
}
