import * as React from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {useToaster} from "../index";

import "./companyfilter.scss"
import "animate.css"
import {useCompanies} from "../../hooks";

export default function CompanyFilter() {
    const {showToaster} = useToaster()
    const {list, setList} = useCompanies()

    const handleChange = (e) => {
        const typed = e.target.value
        let emptyResult = 1
        const newList = list.map(company =>{
            company.show = company.name.toLowerCase().indexOf(typed.toLowerCase()) > -1
            if(company.show)
                emptyResult = 0
            return company
        })
        if(emptyResult === 1)
            showToaster.warning('No matching results!')
        setList(newList)
    }

    return <div className={`companyFilter`}>
        <div className="inputWrapper">
            <FontAwesomeIcon icon={['fas', 'search']} />
            <input type="text" placeholder="Filter by name..." onChange={handleChange} />
        </div>
    </div>
}
