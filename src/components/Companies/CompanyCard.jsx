import * as React from "react"
import {useEffect} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {useToaster} from "../index";

import {LOCATIONS, HEADQUARTERS, SECTORS} from "../../hooks/data"
import "./companyCard.scss"
import "animate.css"
import {useState} from "react";
import {useCompanies} from "../../hooks";

export default function CompanyCard({companyInfo}) {
    const {showToaster} = useToaster()
    const [animate, setAnimate] = useState('animate__animated animate__zoomIn')

    const {list, setList} = useCompanies()

    const {name, discover, siteUrl, linkedIn, match, investors, blurb,
           locations, sectors, stage, daysLeft, id, show, watching} = companyInfo

    const banClick = () => {
        // I'm showing the id instead of call to an api
        console.log(id)

        setAnimate('animate__animated animate__zoomOut')
        const newList = list.filter(company => company.id !== id)
        setTimeout(() => setList(newList), 300)
        showToaster.warning('Your not going to see this company anymore!')
    }
    const watchClick = () => {
        // I'm showing the id instead of call to an api
        console.log(id)

        const newList = list.map(company =>{
            if(company.id === id)
                company.watching = !company.watching
            return company
        })
        setList(newList)

        setAnimate('animate__animated animate__headShake')
        showToaster.info('Company added to your watch list!')
    }
    const interestClick = () => {
        // I'm showing the id instead of call to an api
        console.log(id)

        setAnimate('animate__animated animate__zoomOut')
        const newList = list.filter(company => company.id !== id)
        setTimeout(() => setList(newList), 300)
        showToaster.success('Cheers! you will receive news about this new relation!')
    }
    useEffect(() => {
        setAnimate('animate__animated')
    }, [list])

    return <div className={`companyCard ${animate}`} style={{display: show ? 'block' : 'none'}}>
        <div className="discovered">
            <div className="discoveredBy"><b>DISCOVER</b> / Discovered by {discover}</div>
            <div className="daysLeft">{daysLeft} days left</div>
        </div>
        <div className="company">
            <div className="logo">
                <FontAwesomeIcon icon={['fas', 'city']} />
            </div>
            <div className="info">
                <h2 className="name">{name}</h2>
                <div className="links">
                    {siteUrl && <a href={siteUrl}>Website</a>}
                    {siteUrl && linkedIn ? <span>·</span> : ''}
                    {linkedIn && <a href={linkedIn}><FontAwesomeIcon icon={['fab', 'linkedin']} /></a>}
                </div>
            </div>
            { match > 50 && <div className="match">{match}% match</div>}
        </div>
        <div className="investors colTitle">
            <FontAwesomeIcon icon={['fas', 'user-friends']} /> {investors} investors and corporates looking ar this company
        </div>
        <div className="blurb">
            <div className="colTitle"><FontAwesomeIcon icon={['fas', 'file-alt']} />  Blurb</div>
            <p>{blurb}</p>
        </div>
        <div className="extraInfo">
            {locations.length
                && <div className="locations extraCol">
                <div className="colTitle"><FontAwesomeIcon icon={['far', 'map']} /> Operatin Locations</div>
                {locations.map((location, idx) => {
                    return <div key={idx}>{LOCATIONS[location]}</div>
                })}
                </div>}
            {locations.length
                && <div className="headquarters extraCol">
                <div className="colTitle"><FontAwesomeIcon icon={['fas', 'building']} /> Headquarters</div>
                {locations.map((location, idx) => {
                    return <div key={idx}>{HEADQUARTERS[location]} ({LOCATIONS[location]})</div>
                })}
                </div>}
            {sectors.length
                && <div className="headquarters extraCol">
                <div className="colTitle"><FontAwesomeIcon icon={['fas', 'briefcase']} /> Sectors</div>
                {sectors.map(sector => SECTORS[sector])}
                </div>}
            {stage
                && <div className="stage extraCol">
                <div className="colTitle"><FontAwesomeIcon icon={['fas', 'filter']} /> Stage</div>
                {stage}
                </div>}
        </div>
        <div className="actions">
            <button onClick={banClick}><FontAwesomeIcon icon={['fas', 'ban']} /> Not interested</button>
            <button onClick={watchClick} className={`${watching && 'secondary'}`}><FontAwesomeIcon icon={['fas', `${watching ? 'eye' : 'inbox'}`]} /> {watching ? 'Added to watchlist' : 'Add to watchlist'}</button>
            <button className="primary" onClick={interestClick}><FontAwesomeIcon icon={['far', 'check-circle']} /> I'm interested</button>
        </div>
    </div>
}
