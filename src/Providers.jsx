import * as React from "react"
import {CompaniesContextProvider} from "./context";


export default function CommonProviders({children, ...props}) {
    return <CompaniesContextProvider>
		{children}
    </CompaniesContextProvider>
}
