import {useCallback, useContext} from "react"
import {CompaniesContext} from '../context'
import getData from "./data";

export default function useCompanies() {
    const {state, setState} = useContext(CompaniesContext);

	const updateState = useCallback(newState => {
	    setState(state => ({ ...state, ...newState}))
	}, [setState])

	const setList = useCallback( newList => {
		updateState({ list: newList})
	}, [updateState])

	const updateList = () => {
		setList(getData())
	}

	return {
		list: state.list,
		emptyData: !state.list.length,

		updateState, setList, updateList
	}
}