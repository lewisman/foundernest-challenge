
export default function getData() {
    return COMPANIES.map(company => generateCompany(company))
}

const generateCompany = (name="Real company") => {
    return {
            id: Math.random().toString(36).substr(2, 9),
            name: name,
            siteUrl: 'http://...',
            linkedIn: Math.random() < 0.5 ? 'http://...' : '',
            stage: Math.random() < 0.8 ? 'Seed / Raising USD 1.2M' : '',
            match: Math.floor(Math.random() * 60) + 20,
            daysLeft: Math.floor(Math.random() * 30) + 2,
            discover: 'FounderNest',
            investors: Math.floor(Math.random() * 20) + 1,
            locations: randomListContent(LOCATIONS),
            sectors: randomListContent(SECTORS),
            blurb: 'Having led a cosy life, Georgina finds himself unable to find scheming robots in Oxford. So he sets out to acquire some scheming robots from Cambridge instead.',
            show: true,
            watching: false
        }
}

function randomListContent(index) {
    const result = []
    for (let i = 0; i < Object.keys(index).length; i++) {
        if(Math.random() < 0.5 || i < 1) {
            const newValue = Math.floor(Math.random() * Object.keys(index).length) + 1
            if(result.indexOf(newValue) < 0)
                result.push(newValue)
        }
    }
    return result
}

const COMPANIES = [
    'CHOAM',
    'MomCorp',
    'Sirius Cybernetics Corp',
    'Acme Corp.',
    'Rich Industries',
]

export const LOCATIONS = {
    1: 'United States of America',
    2: 'Spain',
}

export const SECTORS = {
    1: 'Furniture & Household Items',
    2: 'RealStates',
    3: 'Data Science'
}

export const HEADQUARTERS = {
    1: 'San Francisco Bay Area',
    2: 'Madrid'
}