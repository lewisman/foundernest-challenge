# FounderNest Challenge

The main goal is to create a **mock of the dashboard** presented in the business context. ☝️  Please, include also an **estimate of the time you have spent** working on the challenge as a whole and a break-down of this estimate in the main parts you have structured your work in.

More information about [the challenge here](https://www.notion.so/Front-end-developer-Challenge-card-dd652341db4a4a0ea106a0e2a3a965ff)

## 🗺 Road map

<ol>
<li>Create ReactApp in a git repository</li>
<li>Create mock data</li>
<li>Create mock API</li>
<li>Build the crad company component</li>
<li>Create the filter to loock for companies</li>
<li>Build notification component (react-hot-toast?)</li>
</ol>

### ⏲ Estimation time `2H 20m`

Create repository and ReactApp `5 min.`  
Create mock data and structure `20 min.`  
Create mock API `15 min.`  
Build the crad company component `30 min.`   
Card Company actions `30 min.`  
Creat the filter to look for companies `30 min.`  
Build notification component `10 min.`

### ⏰ Tracking Time

#### 12-25-2021
`10:30-10:38` Managing and planing `10 min.`
#### 12-26-2021
`07:53-08:17` Create ReactApp in a git repository `15 min.`
* Spent more time beacause of readme.  

`08:17-09:04` Create mock data and structure `45 min.`  
`10:24-10:58` Build the crad company component `30 min.`
#### 12-27-2021
`09:26-11:09` Card Company actions & Build notification component `1h 40min.`
* Spent more time beacause of styling and notifications.  

`12:43-13:20` Creat the filter to look for companies `30min.`


## Time Spent `3h` approx.